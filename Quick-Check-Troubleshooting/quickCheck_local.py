#!/usr/bin/env python

import os
import subprocess
import array as arr
import glob
import re
from datetime import datetime

# as ph_support njsr12
# quickCheck.py
THISHOST = input("Enter Strat Name: ")
# how tf do i do a sshto as a subprocess
# subprocess.run()
# os.system(f"sshto {THISHOST}")
STRATSPLIT = THISHOST.split('_')
GETVARIANT = re.findall(r'\d+', STRATSPLIT[1])[-1]
os.chdir('../')
os.chdir(f'q/strat')
if os.path.exists(STRATSPLIT[1]) == True:
    STRATPARENT = STRATSPLIT[1]
else:
    STRATPARENT = STRATSPLIT[1].split(GETVARIANT, 1)[0] + GETVARIANT

print("\nQuick Check (Troubleshooting)")
print("1 - check if strategy ran today")
print("2 - check component status")
choice = input("Enter here: ")
print("\n")

if choice == '1':
    os.chdir('../../')
    print("DITO" + os.getcwd())
    os.chdir(f'q/strat/{STRATPARENT}/{THISHOST}/log')
    if os.path.exists('today') == True:
        print(f"Strategy {THISHOST} ran today.")
        # subprocess.run(exit)
        # os.system("exit")
        os.chdir(f'../../../../../q/qbtstrat/{STRATPARENT}/beta_startup/')
        STARTFILE = open(f"{THISHOST}.start", "r")
        print(f"Start File: q/qbtstrat/{STRATPARENT}/beta_startup/{THISHOST}.start")
        print(STARTFILE.read())
    else:
        print(f"Strategy {THISHOST} has no log for today.")
    os.chdir('../')
elif choice == '2':
    os.chdir('../../')
    os.chdir(f'q/strat/{STRATPARENT}/{THISHOST}/log')
    DATETODAY = datetime.today().strftime('%Y%m%d')
    if os.path.exists('today') == True:
        os.chdir('today')
        SMLOG = open(f"{DATETODAY}.SM.log", "r")
        with open(f"{DATETODAY}.SM.log", "r") as f:
            logs = f.readlines()
        print("INIT")
        for log in logs:
            if re.search("INIT", log):
                print(log, end="")
        print("START")
        for log in logs:
            if re.search("START", log):
                print(log, end="")
        print("CRITICAL")
        for log in logs:
            if re.search("CRITICAL", log):
                print(log, end="")
    else:
        print(f"Strategy {THISHOST} has no log for today.")
    os.chdir('../../../../../../')