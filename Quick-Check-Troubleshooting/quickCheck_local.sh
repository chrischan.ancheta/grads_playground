#!/bin/bash

# variables
TODAY="today"
DATETODAY=$(date +%Y%m%d)
choice="0"
STRATHOSTFILE=strat_host_alias.csv

# starting prompt
echo "Quick Check (Troubleshooting)"
echo "1 - check if strategy ran today"
echo "2 - check component status"
echo "3 - exit"
read -p "Enter here: " choice;
echo -e "\n"

# functions
goHere() {
       echo $(pwd)
       read -p "Enter Strat Name: " THISHOST;
       cd "../../"
       # assuming that sshto is successful, current path is /q/strat/parent/stratname/, and below runs:
       checkParent=$( echo $THISHOST | cut -d"_" -f2)
       if [ -d "q/strat/$checkParent" ]; then
           parent=$checkParent
       else
           parent=$( echo $THISHOST | cut -d"_" -f2 | tr -d "[a-ru-z]")
       fi
       cd "q/strat/shared/misc/"
       host=$( grep -w ^$THISHOST $STRATHOSTFILE | cut -d"," -f2 | head -1 );
       cd "../../../../"
       cd "q/strat/$parent/$THISHOST/log"
}

case $choice in
    1) goHere
        if [ -d "$TODAY" ]; then
            echo "Strategy '$THISHOST' ran today."
            cd "../../../../../"
            cd "q/qbtstrat/$parent/beta_startup/"
            echo "Start File: q/qbtstrat/$parent/beta_startup/$(ls $THISHOST.start)"
            cat $THISHOST.start
        else
            echo "Strategy '$THISHOST' has no log for today."
            fi
            cd "../../../"
            cd "ph_ops/trading-support-scripts"
            ;;
    2) goHere
           if [ -d "$TODAY" ]; then
               cd "today"
               echo "INIT"
                   echo $( grep -i init $DATETODAY.SM.log )
                   echo -e "\n"
               echo "START"
                   echo $( grep -i start $DATETODAY.SM.log )
                   echo -e "\n"
               echo "CRITICAL"
                   echo $( grep -i critical $DATETODAY.SM.log )
                   echo -e "\n"
           else
               echo "Strategy '$THISHOST' has no log for today."
           fi
           cd "../../../../../"
           cd "ph_ops/trading-support-scripts"
       ;;
    3) exit
       ;;
esac