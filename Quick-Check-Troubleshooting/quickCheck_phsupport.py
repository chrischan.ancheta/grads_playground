#!/usr/bin/env python

import os
import subprocess
import array as arr
import glob
import re
from datetime import datetime

# as ph_support njsr12
# quickCheck.py
# pwd: /q/strat/st17/as_st17n_sim_beta1_rt
HOST = os.getcwd()
STRATSPLIT = HOST.split('/')
STRATNAME = STRATSPLIT[4]
STRATPARENT = STRATSPLIT[3]
print(f"Strategy Name: {STRATNAME}")

# starting prompt
print("\nQuick Check (Troubleshooting)")
print("1 - check if strategy ran today")
print("2 - check component status")
choice = input("Enter here: ")
print("\n")

if choice == '1':
    os.chdir('../../../')
    os.chdir(f'q/strat/{STRATPARENT}/{STRATNAME}/log')
    if os.path.exists('today') == True:
        print(f"Strategy {STRATNAME} ran today.")
        # to logout of secure shell session and go back as ph_support njsr12
        # subprocess.run(exit)
        # os.system("exit")
        os.chdir(f'/q/qbtstrat/{STRATPARENT}/beta_startup/')
        STARTFILE = open(f"{STRATNAME}.start", "r")
        print(f"Start File: q/qbtstrat/{STRATPARENT}/beta_startup/{STRATNAME}.start")
        print(STARTFILE.read())
    else:
        print(f"Strategy {STRATNAME} has no log for today.")
elif choice == '2':
    os.chdir('../../../')
    os.chdir(f'q/strat/{STRATPARENT}/{STRATNAME}/log')
    DATETODAY = datetime.today().strftime('%Y%m%d')
    if os.path.exists('today') == True:
        os.chdir('today')
        SMLOG = open(f"{DATETODAY}.SM.log", "r")
        with open(f"{DATETODAY}.SM.log", "r") as f:
            logs = f.readlines()
        print("INIT")
        for log in logs:
            if re.search("INIT", log):
                print(log, end="")
        print("START")
        for log in logs:
            if re.search("START", log):
                print(log, end="")
        print("CRITICAL")
        for log in logs:
            if re.search("CRITICAL", log):
                print(log, end="")
    else:
        print(f"Strategy {STRATNAME} has no log for today.")