#!/bin/bash

# variables
TODAY="today"
DATETODAY=$(date +%Y%m%d)
choice="0"

# as ph_support njsr12
# quickCheck.sh
read -p "Enter Strat Name: " THISHOST
$(sshto $THISHOST)
# sshto as_st17n_sim_beta1_rt
# pwd: /q/strat/st17/as_st17n_sim_beta1_rt
STRATNAME=$(echo $(pwd) | cut -d"/" -f5)
STRATPARENT=$(echo $(pwd) | cut -d"/" -f4)
echo "Strategy Name: $STRATNAME"

    # starting prompt
    echo "Quick Check (Troubleshooting)"
    echo "1 - check if strategy ran today"
    echo "2 - check component status"
    read -p "Enter here: " choice;
    echo -e "\n"

    # functions
    goHere() {
        cd "log"
    }

    case $choice in
        1) goHere
        if [ -d "$TODAY" ]; then
                echo "Strategy '$STRATNAME' ran today."
                # to logout of secure shell session and go back as ph_support njsr12
                exit
                cd "/q/qbtstrat/$STRATPARENT/beta_startup/"
                echo "Start File: q/qbtstrat/$parent/beta_startup/$(ls $STRATNAME.start)"
                cat $STRATNAME.start
                cd
        else
                echo "Strategy '$THISHOST' has no log for today."
                # to logout of secure shell session and go back as ph_support njsr12
                exit
                cd
        fi
        ;;
        2) goHere
            if [ -d "$TODAY" ]; then
                cd "today"
                echo "INIT"
                    echo $( grep -i init $DATETODAY.SM.log )
                    echo -e "\n"
                echo "START"
                    echo $( grep -i start $DATETODAY.SM.log )
                    echo -e "\n"
                echo "CRITICAL"
                    echo $( grep -i critical $DATETODAY.SM.log )
                    echo -e "\n"
            else
                echo "Strategy '$THISHOST' has no log for today."
            fi
            # to logout of secure shell session and go back as ph_support njsr12
            exit
            cd
        ;;
    esac